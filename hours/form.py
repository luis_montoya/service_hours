# django
from django import forms
from .models import *


class HoursForm(forms.ModelForm):
	start = forms.TimeField(label='Starts at:')
	end = forms.TimeField(label='Ends at:')