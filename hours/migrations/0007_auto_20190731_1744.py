# Generated by Django 2.2.3 on 2019-07-31 17:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hours', '0006_myactivity'),
    ]

    operations = [
        migrations.AddField(
            model_name='hours',
            name='description',
            field=models.CharField(blank=True, max_length=5000, null=True),
        ),
        migrations.AlterField(
            model_name='myactivity',
            name='date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
