from django.db import models


class Activity(models.Model):
	description = models.CharField(max_length=1000, blank=False, null=False)

	def __str__(self):
		return self.description


class Hours(models.Model):
	start_time = models.TimeField(blank=False, null=False)
	end_time = models.TimeField(blank=False, null=False)
	duration = models.FloatField()
	project = models.CharField(max_length=5000, blank=False, null=False)
	user = models.CharField(max_length=50000, blank=False, null=False)
	comments = models.CharField(max_length=5000, blank=True, null=True) 
	date = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
	description = models.CharField(max_length=5000, blank=True, null=True)

	def __str__(self):
		return self.user


class MyActivity(models.Model):
	date = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
	user = models.CharField(max_length=50000, blank=False, null=False)
	favor_hours = models.FloatField() 
