from django.urls import path, include
from .views import HomeView, RegisterHoursView, RecordsDayArchiveView, ReportsView, EngineerReportsView, EditHoursView

urlpatterns = [
	path('', HomeView.as_view(), name='home'),
	path('record/<str:date>/', RecordsDayArchiveView.as_view(), name="record_day"),
	path('record/<str:date>/<int:pk>/', EditHoursView.as_view(), name='edit'),
	path('<int:pk>/', EditHoursView.as_view(), name='edit'),
	path('update/<str:date>/', EditHoursView.as_view(), name='update'),
	path('register/<str:date>/', RegisterHoursView.as_view(), name='register'),
	path('myactivity/', ReportsView.as_view(), name='reports'),
	path('engineer_activity/', EngineerReportsView.as_view(), name='engineer_activity'),
]
