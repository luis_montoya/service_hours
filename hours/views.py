# Django
from django.shortcuts import render, redirect
from django.db import connections
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum, Max, Count
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.apps import apps

# Models
from .models import *
from projects.models import *

# utils
from datetime import datetime, timedelta
from datetimerange import DateTimeRange
from string import Template
import pandas as pd


def context_vdb():
    cursor = connections['default'].cursor()

    cursor.execute('select * from usuarios where Activo = 1')
    users = cursor.fetchall()

    cursor.execute(
        'select ID, Nombre_proyecto, Numero_Proyecto from proyectos order by ID desc')
    projects = cursor.fetchall()
    internal_projects = Project.objects.all().exclude(id__lte=8)
    projects_lst = []

    InternalProjects = apps.get_model('projects', 'Project')
    internalprojects = InternalProjects.objects.all()

    for internal in internalprojects:
        projects_lst.append({'id':internal.pk, 'name':internal.name, 'number':'{}{}'.format(internal.category, internal.consecutive)})
        print(projects_lst)

    for project in projects:
        projects_lst.append({'id': project[0], 'name': project[1], 'number': project[2]})

    for internal in internal_projects:
        projects_lst.append({'id': internal.id, 'name': internal.name, 'number': '{}{}'.format(internal.category, internal.consecutive)})

    activities = Activity.objects.all()

    return {'users': users, 'projects': projects_lst, 'activities': activities}


def overlap(hours_current_day, register_hours):
    print(hours_current_day['start_time'], hours_current_day['end_time'], register_hours['start_time'],
          register_hours['end_time'])
    time_rage = DateTimeRange(
        start_datetime=str(hours_current_day['start_time']),
        end_datetime=str(hours_current_day['end_time']),
        start_time_format='%H:%M',
        end_time_format='%H:%M'
    )

    user_time = DateTimeRange(
        start_datetime=str(register_hours['start_time']),
        end_datetime=str(register_hours['end_time']),
        start_time_format='%H:%M',
        end_time_format='%H:%M'
    )

    return time_rage.is_intersection(user_time) or user_time.is_intersection(time_rage)


class RecordsDayArchiveView(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):
        curdate = datetime.strptime('{}'.format(kwargs['date']), '%Y-%m-%d')
        hours = Hours.objects.filter(date=curdate, user=request.user.email)

        day_before = (curdate - (timedelta(days=1))).strftime('%Y-%m-%d')
        today = '{} 00:00:00'.format(datetime.now().strftime('%Y-%m-%d'))
        if str(curdate) in today:
            day_after = 'next'
        else:
            day_after = (curdate + (timedelta(days=1))).strftime('%Y-%m-%d')

        params = context_vdb()
        params['hours'] = hours
        params['today'] = curdate
        params['day_before'] = day_before
        params['day_after'] = day_after

        return render(request, 'list_hours.html', params)

    def post(self, request, *args, **kwargs):

        curdate = datetime.strptime('{}'.format(request.POST['date']), '%Y-%m-%d')
        hours = Hours.objects.filter(date=curdate, user=request.user.email)

        day_before = (curdate - (timedelta(days=1))).strftime('%Y-%m-%d')
        today = '{} 00:00:00'.format(datetime.now().strftime('%Y-%m-%d'))
        if str(curdate) in today:
            day_after = 'next'
        else:
            day_after = (curdate + (timedelta(days=1))).strftime('%Y-%m-%d')

        params = context_vdb()
        params['hours'] = hours
        params['today'] = curdate
        params['day_before'] = day_before
        params['day_after'] = day_after

        return render(request, 'list_hours.html', params)


class HomeView(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):

        curdate = datetime.strptime('{}'.format(datetime.now().strftime('%Y-%m-%d')), '%Y-%m-%d')
        hours = Hours.objects.filter(date=curdate, user=request.user.email).order_by('start_time')

        day_before = (datetime.now() - (timedelta(days=1))).strftime('%Y-%m-%d')

        params = context_vdb()
        params['hours'] = hours
        params['today'] = curdate
        params['day_before'] = day_before
        params['day_after'] = 'next'

        return render(request, 'list_hours.html', params)

    def post(self, request, *args, **kwargs):

        curdate = datetime.strptime('{}'.format(request.POST['date']), '%Y-%m-%d')
        hours = Hours.objects.filter(date=curdate, user=request.user.email).order_by('start_time')

        day_before = (curdate - (timedelta(days=1))).strftime('%Y-%m-%d')
        today = '{} 00:00:00'.format(datetime.now().strftime('%Y-%m-%d'))
        if str(curdate) in today:
            day_after = 'next'
        else:
            day_after = (curdate + (timedelta(days=1))).strftime('%Y-%m-%d')

        params = context_vdb()
        params['hours'] = hours
        params['today'] = curdate
        params['day_before'] = day_before
        params['day_after'] = day_after

        return render(request, 'list_hours.html', params)


class EditHoursView(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):

        hours = Hours.objects.all()
        edited_hours = Hours.objects.get(pk=kwargs['pk'])

        params = context_vdb()
        params['hours'] = hours
        params['curdate'] = edited_hours.date
        params['id'] = edited_hours.pk
        params['start'] = edited_hours.start_time
        params['end'] = edited_hours.end_time
        params['n_project'] = edited_hours.project
        params['activity_'] = edited_hours.description
        params['comment'] = edited_hours.comments

        return render(request, 'edit_hours.html', params)

    def post(self, request, *args, **kwargs):

        user = request.user.email
        cursor = connections['default'].cursor()

        cursor.execute('select * from usuarios where Activo = 1')
        users = cursor.fetchall()

        cursor.execute(
            'select ID, Nombre_proyecto, Numero_Proyecto from proyectos where Estatus_Actual <> "Cancelado" and '
            'Estatus_Actual <> "Entregado" order by ID desc')
        projects = cursor.fetchall()
        projects_lst = []

        for project in projects:
            projects_lst.append({'id': project[0], 'name': project[1], 'number': project[2]})

        activities = Activity.objects.all()

        curdate = request.POST['date'].split(' ')
        hours = Hours.objects.get(pk=request.POST['id'])

        # FMT = '%H:%M:%S'
        FMT = '%H:%M'

        start = datetime.strptime(request.POST['startTime'], FMT)
        end = datetime.strptime(request.POST['endTime'], FMT)

        tmpStart = datetime.strptime('{}'.format(hours.start_time.strftime(FMT)), FMT)
        tmpEnd = datetime.strptime('{}'.format(hours.end_time.strftime(FMT)), FMT)

        difference = end - start
        duration = round(difference.total_seconds() / 3600, 2)

        acummulated = Hours.objects.filter(user=request.user.email, date=curdate[0]).values('date').annotate(
            Sum('duration'))
        acum_hours = 0
        for i in acummulated:
            acum_hours = i['duration__sum']

        acum_hours = acum_hours - hours.duration
        if request.POST['activity'] != 23 and request.POST['activity'] != 3:
            acum_hours = acum_hours + duration

        if end <= start:
            return render(request, 'add_hours.html',
                          {'projects': projects_lst, 'activities': activities, 'curdate': curdate[0],
                           'error': 'Start time needs to be higher than end time!'})
        if request.POST['project'] == 'no-selected':
            return render(request, 'add_hours.html',
                          {'projects': projects_lst, 'activities': activities, 'curdate': curdate[0],
                           'error': 'Please choose a project!'})
        if request.POST['activity'] == 'no-selected':
            return render(request, 'add_hours.html',
                          {'projects': projects_lst, 'activities': activities, 'curdate': curdate[0],
                           'error': 'Please choose an activity!'})
        if acum_hours > 24:
            return render(request, 'add_hours.html',
                          {'projects': projects_lst, 'activities': activities, 'curdate': curdate[0],
                           'error': 'It is impossible to work more than 24 hrs a day!'})

        hours_current_day = Hours.objects.filter(user=request.user.email, date=curdate[0])

        overlaped = False
        overlaped_hours = None

        for row in hours_current_day:
            overlaped = overlap({'start_time': row.start_time, 'end_time': row.end_time},
                                {'start_time': '{}:{}'.format(start.hour, start.minute),
                                 'end_time': '{}:{}'.format(end.hour, end.minute)})
            if overlaped:
                overlaped_hours = '{} {}'.format(row.start_time, row.end_time)

        if overlaped and tmpStart != start and tmpEnd != end:
            return render(
                request,
                'add_hours.html',
                {
                    'projects': projects_lst,
                    'activities': activities,
                    'curdate': curdate[0],
                    'error': 'You could not overlap hours, you try to add an activity between {}'.format(
                        overlaped_hours)
                }
            )

        hours.date = curdate[0]
        hours.start_time = request.POST['startTime']
        hours.end_time = request.POST['endTime']
        hours.project = request.POST['project']
        hours.comments = request.POST['comment']
        hours.user = user
        hours.description = request.POST['activity']
        hours.duration = duration

        hours.save()

        if request.POST['activity'] != 3:
            try:
                my_activity = MyActivity.objects.get(user=request.user.email, date=curdate[0])
                my_activity.date = curdate[0]
                my_activity.user = user
                my_activity.favor_hours = 0
                my_activity.save()
            except ObjectDoesNotExist:
                mact = MyActivity()
                mact.date = curdate[0]
                mact.user = user
                mact.favor_hours = 0
                mact.save()

            if acum_hours > 10 and request.POST['activity'] != 23:
                print(curdate[0])
                my_activity = MyActivity.objects.get(user=request.user.email, date=curdate[0])
                favor = round(acum_hours - 10, 2)
                my_activity.favor_hours = favor
                my_activity.save()

            if request.POST['activity'] == 23:
                my_activity = MyActivity.objects.get(user=request.user.email, date=curdate[0])
                favor = round(acum_hours - duration, 2)
                my_activity.favor_hours = favor
                my_activity.save()

        return redirect('record_day', date=curdate[0])


class RegisterHoursView(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):

        hours = Hours.objects.all()
        params = context_vdb()
        params['hours'] = hours
        params['curdate'] = kwargs['date']

        return render(request, 'add_hours.html', params)

    def post(self, request, *args, **kwargs):
        # activities = Activity.objects.get(pk=request.POST['activity'])
        flag = False
        user = request.user.email
        cursor = connections['default'].cursor()

        cursor.execute('select * from usuarios where Activo = 1')
        users = cursor.fetchall()

        cursor.execute(
            'select ID, Nombre_proyecto, Numero_Proyecto from proyectos where Estatus_Actual <> "Cancelado" and '
            'Estatus_Actual <> "Entregado" order by ID desc')
        projects = cursor.fetchall()
        projects_lst = []

        for project in projects:
            projects_lst.append({'id': project[0], 'name': project[1], 'number': project[2]})

        activities = Activity.objects.all()

        curdate = request.POST['date'].split(' ')
        hours = Hours()

        hours.date = curdate[0]
        hours.start_time = request.POST['startTime']
        hours.end_time = request.POST['endTime']
        hours.project = request.POST['project']
        hours.comments = request.POST['comment']
        hours.user = user
        hours.description = request.POST['activity']

        # FMT = '%H:%M:%S'
        FMT = '%H:%M'

        start = datetime.strptime(request.POST['startTime'], FMT)
        end = datetime.strptime(request.POST['endTime'], FMT)

        difference = end - start
        duration = round(difference.total_seconds() / 3600, 2)
        hours.duration = duration

        acummulated = Hours.objects.filter(user=request.user.email, date=curdate[0]).values('date').annotate(
            Sum('duration'))
        acum_hours = 0
        for i in acummulated:
            acum_hours = i['duration__sum']

        if request.POST['activity'] != 23 and request.POST['activity'] != 3:
            acum_hours = acum_hours + duration

        if end <= start:
            return render(request, 'add_hours.html',
                          {'projects': projects_lst, 'activities': activities, 'curdate': curdate[0],
                           'error': 'Start time needs to be higher than end time!'})
        if request.POST['project'] == 'no-selected':
            return render(request, 'add_hours.html',
                          {'projects': projects_lst, 'activities': activities, 'curdate': curdate[0],
                           'error': 'Please choose a project!'})
        if request.POST['activity'] == 'no-selected':
            return render(request, 'add_hours.html',
                          {'projects': projects_lst, 'activities': activities, 'curdate': curdate[0],
                           'error': 'Please choose an activity!'})
        if acum_hours > 24:
            return render(request, 'add_hours.html',
                          {'projects': projects_lst, 'activities': activities, 'curdate': curdate[0],
                           'error': 'It is impossible to work more than 24 hrs a day!'})

        hours_current_day = Hours.objects.filter(user=request.user.email, date=curdate[0])

        overlaped = False
        overlaped_hours = None

        for row in hours_current_day:
            overlaped = overlap({'start_time': row.start_time, 'end_time': row.end_time},
                                {'start_time': '{}:{}'.format(start.hour, start.minute),
                                 'end_time': '{}:{}'.format(end.hour, end.minute)})
            if overlaped:
                overlaped_hours = '{} {}'.format(row.start_time, row.end_time)

        if overlaped:
            return render(request, 'add_hours.html',
                          {'projects': projects_lst, 'activities': activities, 'curdate': curdate[0],
                           'error': 'You could not overlap hours, you try to add an activity between {}'.format(
                               overlaped_hours)})

        hours.save()

        hours = Hours.objects.all()

        if request.POST['activity'] != 3:
            try:
                my_activity = MyActivity.objects.get(user=request.user.email, date=curdate[0])
                my_activity.date = curdate[0]
                my_activity.user = user
                my_activity.favor_hours = 0
                my_activity.save()
            except ObjectDoesNotExist:
                mact = MyActivity()
                mact.date = curdate[0]
                mact.user = user
                mact.favor_hours = 0
                mact.save()

            if acum_hours > 10 and request.POST['activity'] != 23:
                print(curdate[0])
                my_activity = MyActivity.objects.get(user=request.user.email, date=curdate[0])
                favor = round(acum_hours - 10, 2)
                my_activity.favor_hours = favor
                my_activity.save()

            if request.POST['activity'] == 23:
                my_activity = MyActivity.objects.get(user=request.user.email, date=curdate[0])
                favor = round(acum_hours - duration, 2)
                my_activity.favor_hours = favor
                my_activity.save()

        return redirect('record_day', date=curdate[0])


class ReportsView(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):
        categories = []
        series = []
        dic_projects = {}
        dic_activities = {}

        data = Hours.objects.filter(user=request.user.email).values('project').annotate(Sum('duration'))
        lst_hours = MyActivity.objects.filter(user=request.user.email).values('user').annotate(Sum('favor_hours'))
        max_activity = Hours.objects.filter(user=request.user.email).values('description').annotate(
            total=Count('description')).order_by('total')

        for i in data:
            categories.append(i['project'])
            series.append(round(i['duration__sum'], 2))
            dic_projects[i['project']] = i['duration__sum']

        if len(dic_projects) > 0:
            maximum = max(dic_projects, key=dic_projects.get)
        else:
            maximum = 0

        for activity in max_activity:
            dic_activities[activity['description']] = activity['total']

        if len(dic_activities) > 0:
            act = max(dic_activities, key=dic_activities.get)
        else:
            act = 0

        favor_hours = 0
        for x in lst_hours:
            favor_hours = x['favor_hours__sum']

        return render(
            request,
            'reports.html',
            {
                'categories': categories,
                'series': series,
                'favor_hours': favor_hours,
                'max_project': maximum,
                'max_activity': act
            }
        )


class EngineerReportsView(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):
        categories = []
        series = []
        dic_projects = {}
        dic_activities = {}

        data = Hours.objects.all().values('project').annotate(Sum('duration'))
        userList = User.objects.values()

        for i in data:
            categories.append(i['project'])
            series.append(round(i['duration__sum'], 2))
            dic_projects[i['project']] = i['duration__sum']

        for u in userList:
            u["profile_picture"] = '{}.jpeg'.format(u['email'])
            u["projectQty"] = Hours.objects.filter(user=u['email']).count()

            lst_hours = MyActivity.objects.filter(user=u['email']).values('user').annotate(Sum('favor_hours'))
            u["favor_hours"] = 0
            for x in lst_hours:
                u["favor_hours"] = x['favor_hours__sum']

        return render(
            request,
            'engineer_reports.html',
            {
                'categories': categories,
                'series': series,
                'users': userList,
            }
        )
