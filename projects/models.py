from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    STATUS = (
        ('O', 'Open'),
        ('C', 'Closed'),
        ('H', 'On Hold'),
    )

    category = models.CharField(max_length=240, blank=False, null=False)
    consecutive = models.CharField(max_length=240, blank=True, null=True)
    name = models.CharField(max_length=900, blank=False, null=False)
    register_date = models.DateTimeField(auto_now_add=True)
    customer = models.CharField(max_length=900, blank=False, null=False)
    contact = models.CharField(max_length=900, blank=False, null=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=1, choices=STATUS),
    scheduled_start_date = models.DateField(auto_now_add=False, auto_now=False)
    scheduled_end_date = models.DateField(auto_now_add=False, auto_now=False)
    progress = models.CharField(max_length=10, blank=True, null=True)
    actual_start_date = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
    actual_end_date = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
    difference = models.IntegerField(blank=True, null=True)
    comments = models.CharField(max_length=5000, blank=True, null=True)

    def __str__(self):
        return '{}{}'.format(self.category, self.name)
