from django.urls import path
from .views import ProjectView, NewProjectView

urlpatterns = [
    path('', ProjectView.as_view(), name='project'),
    path('newProject/', NewProjectView.as_view(), name='newProject')
]
