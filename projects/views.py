# Django
from django.shortcuts import render, redirect
from django.db import connections
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

# Models
from .models import *


class ProjectView(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):
        projects = Project.objects.all().exclude(id__lte=8)

        return render(request, 'project.html', {'projects': projects})


class NewProjectView(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):
        users = User.objects.values()
        last_project = Project.objects.latest('id')
        next_project_number = int(last_project.consecutive) + 1
        consecutive = str(next_project_number)

        return render(request, 'add_project.html', locals())

    def post(self, request, *args, **kwargs):

        category = request.POST['category']
        project_number = request.POST['project_number']
        project_name = request.POST['project_name']
        customer = request.POST['customer']
        contact = request.POST['contact']
        owner = request.POST['owner']
        scheduled_start = request.POST['scheduled_StartDate']
        scheduled_end = request.POST['scheduled_EndDate']
        comments = request.POST['comment']

        new_project = Project()
        new_project.category = category
        new_project.consecutive = project_number
        new_project.name = project_name
        new_project.customer = customer
        new_project.contact = contact
        new_project.owner_id = owner
        new_project.scheduled_start_date = scheduled_start
        new_project.scheduled_end_date = scheduled_end
        new_project.comments = comments
        new_project.save()

        return redirect('project')
