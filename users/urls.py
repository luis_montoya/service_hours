from django.urls import path, include
from .views import LoginView, LogOutView, ProfileView

urlpatterns=[
	path('login/', LoginView.as_view(), name='login'),
	path('logout/', LogOutView.as_view(), name='logout'),
	path('profile/', ProfileView.as_view(), name='profile'),
]
