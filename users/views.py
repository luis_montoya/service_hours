# Django
from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import connections


class LoginView(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'login.html')

    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('home')
        else:
            return render(request, 'login.html', {'error': 'Invalid username and password'})


class LogOutView(LoginRequiredMixin, View):
    login_url = '/users/login/'

    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('login')


class ProfileView(LoginRequiredMixin, View):
    login_url = 'login'

    def get(self, request, *args, **kwargs):
        cursor = connections['default'].cursor()

        cursor.execute('select d.Departamento, p.Puesto as dep from usuarios as u inner join deptos as d on u.Departamento = d.idDep inner join puestos as p on u.Puesto = p.id where u.Usuario = "{}"'.
                       format(request.user.email)
                       )
        users = cursor.fetchone()

        return render(request, 'profile.html', {'depto': users[0], 'Position': users[1]})
